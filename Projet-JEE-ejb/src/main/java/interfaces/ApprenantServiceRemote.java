package interfaces;

import javax.ejb.Remote;

@Remote
public interface ApprenantServiceRemote {
	public void ajouterApprenant();
	public void ajouterCentre();
}
